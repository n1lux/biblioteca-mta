from django.contrib.auth import authenticate, login, logout
from django.http import JsonResponse
from django.shortcuts import render
from django.template.loader import render_to_string
from django.views import View
from django.contrib.auth.mixins import LoginRequiredMixin

# Create your views here.
from core.forms import BookForm, PersonForm, LoanForm
from core.models import Book, Person, BookLoan


class LoginView(View):
    def get(self, request):
        return render(request, 'login/login.html')


    def post(self, request):
        user = authenticate(username=request.POST['username'], password=request.POST["password"])
        if user is not None:
            login(request, user)
            return render(request, 'home/index.html')
        else:
            return render(request, 'login/login.html')


class LogoutView(View):
    def get(self, request):
        logout(request)
        return render(request, 'login/login.html')


class Index(LoginRequiredMixin, View):
    login_url = "/login/"
    redirect_field_name = 'redirect_to'

    def get(self, request):
        return render(request, 'home/index.html')


class Books(LoginRequiredMixin, View):
    login_url = "/login/"
    redirect_field_name = 'redirect_to'

    def get(self, request):
        context = {'books': Book.objects.all()}
        return render(request, 'books/book.html', context=context)


class BooksAjax(LoginRequiredMixin, View):
    login_url = "/login/"
    redirect_field_name = 'redirect_to'

    def get(self, request):
        data = dict()
        form = BookForm()
        context = {'form': form}
        data['html_book_form'] = render_to_string('books/includes/partial-book-form.html',
                                                  context,
                                                  request=request)

        return JsonResponse(data)

    def post(self, request):
        data = dict()
        form = BookForm(request.POST)
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            books = Book.objects.all()
            data['html_book_table'] = render_to_string('books/includes/partial-books-table.html',
                                                       {'books': books}
                                                       )
        else:
            data['form_is_valid'] = False

        context = {'form': form}
        data['html_book_form'] = render_to_string('books/includes/partial-book-form.html',
                                                  context,
                                                  request=request)

        return JsonResponse(data)


class Persons(LoginRequiredMixin, View):
    login_url = "/login/"
    redirect_field_name = 'redirect_to'

    def get(self, request):
        context = {'persons': Person.objects.all()}
        return render(request, 'persons/person.html', context=context)


class PersonsAjax(LoginRequiredMixin, View):
    login_url = "/login/"
    redirect_field_name = 'redirect_to'

    def get(self, request):
        data = dict()
        form = PersonForm()
        context = {'form': form}
        data['html_person_form'] = render_to_string('persons/includes/partial-person-form.html',
                                                  context,
                                                  request=request)

        return JsonResponse(data)

    def post(self, request):
        data = dict()
        form = PersonForm(request.POST)
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            persons = Person.objects.all()
            data['html_person_table'] = render_to_string('persons/includes/partial-person-table.html',
                                                       {'persons': persons}
                                                       )
        else:
            data['form_is_valid'] = False

        context = {'form': form}
        data['html_person_form'] = render_to_string('persons/includes/partial-person-form.html',
                                                  context,
                                                  request=request)

        return JsonResponse(data)


class Loans(LoginRequiredMixin, View):
    login_url = "/login/"
    redirect_field_name = 'redirect_to'

    def get(self, request):
        context = {'loans': BookLoan.objects.all()}
        return render(request, 'loans/loan.html', context=context)


class LoansAjax(LoginRequiredMixin, View):
    login_url = "/login/"
    redirect_field_name = 'redirect_to'

    def get(self, request):
        data = dict()
        form = LoanForm()
        context = {'form': form}
        data['html_loan_form'] = render_to_string('loans/includes/partial-loan-form.html',
                                                  context,
                                                  request=request)

        return JsonResponse(data)

    def post(self, request):
        data = dict()
        form = LoanForm(request.POST)
        books_already_loan = list()
        if form.is_valid():
            books = form.cleaned_data.get('books')

            for book in books:
                if BookLoan.has_book_loan(book=book):
                    books_already_loan.append("Livro {} ja emprestado".format(book.name))

            if not books_already_loan:
                form.save()
                data['form_is_valid'] = True
                loans = BookLoan.objects.all()
                data['html_loan_table'] = render_to_string('loans/includes/partial-loan-table.html',
                                                       {'loans': loans})
            else:
                for error in books_already_loan:
                    form.add_error(None, error)
                data['form_is_valid'] = False
        else:
            data['form_is_valid'] = False

        context = {'form': form}
        data['html_loan_form'] = render_to_string('loans/includes/partial-loan-form.html',
                                                  context,
                                                  request=request)

        return JsonResponse(data)