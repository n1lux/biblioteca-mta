from django.conf.urls import url, include
from core.views import Index, Books, BooksAjax, Persons, PersonsAjax, Loans, LoansAjax

urlpatterns = [
    url(r'^$', Index.as_view(), name='index'),

    url(r'^books/', include([
        url(r'^$', Books.as_view(), name='books'),
        url(r'^create-book/$', BooksAjax.as_view(), name='create-book'),
    ])),

    url(r'^persons/', include([
        url(r'^$', Persons.as_view(), name='persons'),
        url(r'^create-book/$', PersonsAjax.as_view(), name='create-person'),
    ])),

    url(r'^loans/', include([
        url(r'^$', Loans.as_view(), name='loans'),
        url(r'^create-book/$', LoansAjax.as_view(), name='create-loan'),
    ])),
]

