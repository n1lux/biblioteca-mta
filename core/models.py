from datetime import timedelta

from django.db import models
from django.utils import timezone


# Create your models here.
from core.exceptions import BookLoanExpection


class BookManager(models.Manager):
    def create_book(self, name, pages, author, copies):
        book = self.create(name=name, pages=pages, author=author, copies=copies)
        return book


class Book(models.Model):
    name = models.CharField(max_length=40, null=False, blank=False, verbose_name='nome')
    pages = models.PositiveIntegerField(null=True, blank=True)
    author = models.CharField(max_length=30, null=False, blank=False)
    created_at = models.DateTimeField(auto_now_add=True)
    copies = models.PositiveSmallIntegerField(null=False, blank=False, default=0, verbose_name='copias')

    objects = BookManager()

    class Meta:
        verbose_name = 'livro'
        verbose_name_plural = 'livros'
        unique_together = ('name', 'author')


    def __str__(self):
        return "{}".format(self.name)

    @classmethod
    def create(cls, **kwargs):
        model = cls.objects.create_book(**kwargs)
        return model


class PersonManager(models.Manager):
    def create_person(self, name):
        person = self.create(name=name)
        return person


class Person(models.Model):
    name = models.CharField(max_length=30, null=False, blank=False)

    objects = PersonManager()

    class Meta:
        verbose_name = 'usuario'
        verbose_name_plural = 'usuarios'

    def __str__(self):
        return "{}".format(self.name)

    @classmethod
    def create(cls, **kwargs):
        model = cls.objects.create_person(**kwargs)
        return model


class BookLoan(models.Model):
    EXPIRES_DEFAULT_DAY = 3

    books = models.ManyToManyField(Book, related_name='_loans', verbose_name='livro')
    persons = models.ManyToManyField(Person, related_name='_loans', verbose_name='usuario')
    created_at = models.DateTimeField(auto_now_add=True)
    expires_at = models.DateTimeField(null=False, blank=False, default=timezone.now() + timedelta(EXPIRES_DEFAULT_DAY))

    @classmethod
    def create(cls, expires_at=None):
        obj = BookLoan.objects.create()
        return obj


    @classmethod
    def has_book_loan(cls, book):
        return cls.objects.filter(books=book).count() >= 1

    @property
    def loan_books(self):
        return self.books.all()

    @property
    def loan_persons(self):
        return self.persons.all()