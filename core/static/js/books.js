$(function(){
    $('.js-btn-create-book').click(function(){
        $.ajax({
          url: $(this).attr('data-url'),
          type: 'GET',
          dataType: 'json',
          beforeSend: function () {
            $("#modal-book").modal("show");
          },
          success: function (data) {
            $("#modal-book .modal-content").html(data.html_book_form);
          }
        });
    });

    $("#modal-book").on("submit", ".js-book-create-form", function () {
        var form = $(this);
        $.ajax({
          url: form.attr("action"),
          data: form.serialize(),
          type: form.attr("method"),
          dataType: 'json',
          success: function (data) {
            if (data.form_is_valid) {
              $("#book-table tbody").html(data.html_book_table);
              $("#modal-book").modal("hide");
            }
            else {
                console.log('aqui');
                $("#modal-book .modal-content").html(data.html_book_form);
            }
          }
        });
        return false;
     });
});