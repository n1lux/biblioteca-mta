$(function(){
    $('.js-btn-create-person').click(function(){
        $.ajax({
          url: $(this).attr('data-url'),
          type: 'GET',
          dataType: 'json',
          beforeSend: function () {
            $("#modal-person").modal("show");
          },
          success: function (data) {
            $("#modal-person .modal-content").html(data.html_person_form);
          }
        });
    });

    $("#modal-person").on("submit", ".js-person-create-form", function () {
        var form = $(this);
        $.ajax({
          url: form.attr("action"),
          data: form.serialize(),
          type: form.attr("method"),
          dataType: 'json',
          success: function (data) {
            if (data.form_is_valid) {
              $("#person-table tbody").html(data.html_person_table);
              $("#modal-person").modal("hide");
            }
            else {
              $("#modal-person .modal-content").html(data.html_person_form);
            }
          }
        });
        return false;
     });
});