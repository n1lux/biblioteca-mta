$(function(){
    $('.js-btn-create-loan').click(function(){
        $.ajax({
          url: $(this).attr('data-url'),
          type: 'GET',
          dataType: 'json',
          beforeSend: function () {
            $("#modal-loan").modal("show");
          },
          success: function (data) {
            $("#modal-loan .modal-content").html(data.html_loan_form);
          }
        });
    });

    $("#modal-loan").on("submit", ".js-loan-create-form", function () {
        var form = $(this);
        $.ajax({
          url: form.attr("action"),
          data: form.serialize(),
          type: form.attr("method"),
          dataType: 'json',
          success: function (data) {
            if (data.form_is_valid) {
              $("#loan-table tbody").html(data.html_loan_table);
              $("#modal-loan").modal("hide");
            }
            else {
              $("#modal-loan .modal-content").html(data.html_loan_form);
            }
          }
        });
        return false;
     })
});