from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.shortcuts import resolve_url as r


class TestIndexHtml(TestCase):
    def setUp(self):
        user = User.objects.create(username='testuser')
        user.set_password('12345')
        user.save()
        self.client = Client()
        self.client.login(username='testuser', password='12345')

    def test_home_link(self):
        resp = self.client.get(r('index'))
        self.assertContains(resp, '<a class="nav-link" href="{}">Emprestimos'.format(r('loans')))

    def test_book_link(self):
        resp = self.client.get(r('index'))
        self.assertContains(resp, 'a class="nav-link" href="{}">Livros</a>'.format(r('books')))

    def test_person_link(self):
        resp = self.client.get(r('index'))
        self.assertContains(resp, 'a class="nav-link" href="{}">Usuarios</a>'.format(r('persons')))