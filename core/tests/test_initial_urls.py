from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.shortcuts import resolve_url as r


class TestIndexUrl(TestCase):
    def setUp(self):
        user = User.objects.create(username='testuser')
        user.set_password('12345')
        user.save()
        self.client = Client()
        self.client.login(username='testuser', password='12345')

    def test_get_index(self):
        res = self.client.get(r('index'))
        self.assertEqual(res.status_code, 200)
        self.assertTemplateUsed(res, 'home/index.html')

    def test_get_book(self):
        res = self.client.get(r('books'))
        self.assertEqual(res.status_code, 200)
        self.assertTemplateUsed(res, 'books/book.html')

    def test_get_book(self):
        res = self.client.get(r('login'))
        self.assertEqual(res.status_code, 200)
        self.assertTemplateUsed(res, 'login/login.html')