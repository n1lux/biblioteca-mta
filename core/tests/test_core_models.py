from django.test import TestCase, TransactionTestCase
from core.models import Book, Person, BookLoan


class TestBooks(TestCase):

    def test_create_book(self):
        data = {"name": "Python Fluent",
                "pages": 435,
                "author": "Luciano Ramalho",
                "copies": 3}

        book = Book.create(**data)
        self.assertIsInstance(book, Book)
        self.assertEqual(book.name, "Python Fluent")
        self.assertEqual(book.pages, 435)
        self.assertEqual(book.author, "Luciano Ramalho")
        self.assertEqual(book.copies, 3)


class TestPerson(TestCase):
    def test_create_person(self):
        name = "Guido Van"
        person = Person.objects.create(name=name)
        self.assertIsInstance(person, Person)
        self.assertEqual(person.name, name)


class TestBookLoan(TransactionTestCase):
    BOOKS = [{"name": "Python Fluent",
                "pages": 435,
                "author": "Luciano Ramalho",
                "copies": 3
              },

             {"name": "Python Data Scienc",
              "pages": 435,
              "author": "Luciano Ramalho",
              "copies": 3
              },

             {"name": "Python Design Patterns",
              "pages": 435,
              "author": "Gang of four",
              "copies": 3
              }
             ]
    PERSONS = [
        {'name': 'David Gilmour'},
        {'name': 'Dave Grhol'},
        {'name': 'Kurt Kobain'}
    ]

    def setUp(self):
        self.loan = BookLoan.create()
        self.books = [Book.create(**b) for b in self.BOOKS]
        self.persons = [Person.create(**p) for p in self.PERSONS]

    def tearDown(self):
        self.loan.delete()
        for book in self.books:
            book.delete()

        for person in self.persons:
            person.delete()


    def test_create_loan_one_book_person(self):
        self.loan.persons.add(self.persons[0])
        self.loan.books.add(self.books[0])
        self.loan.save()
        self.assertEqual(BookLoan.objects.filter(books=self.books[0]).count(), 1)
        self.assertEqual(BookLoan.objects.filter(persons=self.persons[0]).count(), 1)

    def test_create_loan_many_book_person(self):

        self.loan.persons.set(self.persons)
        self.loan.books.set(self.books)
        self.assertEqual(BookLoan.objects.count(), 1)
        self.assertEqual(self.loan.books.count(), 3)
        self.assertEqual(self.loan.persons.count(), 3)

    def test_has_book_loan(self):
        self.loan.persons.add(self.persons[0])
        self.loan.books.add(self.books[0])
        self.assertEqual(BookLoan.has_book_loan(book=self.books[0]), 1)


