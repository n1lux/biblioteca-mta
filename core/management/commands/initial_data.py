from django.core.management import BaseCommand
from django.core.exceptions import ObjectDoesNotExist


def _create_user():
    from django.contrib.auth.models import User
    try:
        user = User.objects.get(username='developer')
        print("Login. username: {} password: {}".format(user.username, 'mta@123'))
    except ObjectDoesNotExist:
        user = User.objects.create_user('developer', 'dev@mta.com', 'mta@123')
        print("User login created. Login it. username: {} password: {}".format(user.username, user.password))

    return user


class Command(BaseCommand):
    help = 'Fake initial data'

    def handle(self, *args, **options):
        _create_user()