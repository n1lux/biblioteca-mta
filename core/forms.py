from django.forms import ModelForm
from core.models import Book, Person, BookLoan


class BookForm(ModelForm):
    class Meta:
        model = Book
        fields = ('name', 'author', 'copies')

    def clean_name(self):
        return self.cleaned_data['name'].capitalize()


class PersonForm(ModelForm):
    class Meta:
        model = Person
        fields = ('name',)

    def clean_name(self):
        return self.cleaned_data['name'].capitalize()


class LoanForm(ModelForm):

    class Meta:
        model = BookLoan
        fields = ('books', 'persons')



