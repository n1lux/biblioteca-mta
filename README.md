# Biblioteca MTA

Projeto para cadastro de usuários e livros na biblioteca MTA.

## Como preparar a maquina
### Pre-requisitos
1. Instale uma maquina com sistema Ubuntu
2. Instale o postgresql 9.5

### Criação da base da dados
2. Faça login com usuário postgres no shell do ubuntu.
3. Crie o usuário admin 
4. Crie o database de nome biblioteca com o owner admin.
5. Certifique que o database biblioteca foi criado corretamente.
6. Altere a senha do usuário admin para "teste@123"
7. De os privilegios ao database biblioteca para o usuario admin.
8. De a permissão de criar databases para o usuário admin. (Necessario para executar os teste)

```console
su - postgres

createuser admin

createdb biblioteca --owner admin

psql -l

psql -c "ALTER USER admin WITH PASSWORD 'teste@123'"

psql -c "GRANT ALL PRIVILEGES ON DATABASE biblioteca to admin;"

psql -c "ALTER USER admin CREATEDB;"
```


## Como criar o ambiente?

1. Clone o repositório
2. Crie um virtualenv com Python 3.6
3. Ative o seu virtualev.
4. Instale as dependências.
5. Rode as migrações
5. Execute os testes.
6. Inicialize os dados de acesso (User: developer) para login.

```console
git clone https://n1lux@bitbucket.org/n1lux/biblioteca-mta.git mta_biblioteca
cd mta_biblioteca
python3 -m venv .mta
source .mta/bin/activate
pip install -r requirements.txt
python manage.py makemigrations
python manage.py migrate
python manage.py test
python manage.py initial_data
python manage.py collectstatic
```

## Como executar o servidor

```console
python manage.py runserver
```


## Como utilizar
```console
acesse : http://localhost:8000
entre com usuario: developer senha: mta@123
```
